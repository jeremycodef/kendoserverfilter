using System;
using System.Dynamic;
using System.IO;
using KendoServerSide;
using Newtonsoft.Json;
using NUnit.Framework;

namespace TestKendoFilters
{
    public class FilterTest
    {
        private string simpleAndJson { get; set; }
        private string simpleOrJson { get; set; }
        private string andNestedOrJson { get; set; }
        private string tripleOrJson { get; set; }
        [SetUp]
        public void Setup()
        {
            simpleAndJson = File.ReadAllText("TestJson/SimpleAnd.json");
            simpleOrJson = File.ReadAllText("TestJson/SimpleOr.json");
            andNestedOrJson = File.ReadAllText("TestJson/AndNestedOr.json");
            tripleOrJson = File.ReadAllText("TestJson/TripleOr.json");
        }

        [Test]
        public void TestSimpleAnd()
        {
            Filter container = JsonConvert.DeserializeObject<Filter>(simpleAndJson);
            string expected = "LastName = 'White' AND Age >= 20";
            Assert.AreEqual(expected, container.GenerateSql());
        }
        [Test]
        public void TestSimpleAndBothStrings()
        {
            Filter container = JsonConvert.DeserializeObject<Filter>(simpleAndJson);
            container.filters[1].value = "20";
            string expected = "LastName = 'White' AND Age >= '20'";
            Assert.AreEqual(expected, container.GenerateSql());
        }
        [Test]
        public void TestSimpleOr()
        {
            Filter container = JsonConvert.DeserializeObject<Filter>(simpleOrJson);
            string expected = "LastName = 'White' OR Age >= 20";
            Assert.AreEqual(expected, container.GenerateSql());
        }

        [Test]
        public void TestAndNestedOr()
        {
            Filter container = JsonConvert.DeserializeObject<Filter>(andNestedOrJson);
            string expected = "LastName = 'White' AND Age >= 20 AND FirstName = 'Mary' OR City = 'London'";
            Assert.AreEqual(expected, container.GenerateSql());
        }

        [Test]
        public void TestTripleOr()
        {
            Filter container = JsonConvert.DeserializeObject<Filter>(tripleOrJson);
            string expected = "FirstName = 'Mary' OR City = 'London' OR LastName = 'Ted'";
            Assert.AreEqual(expected, container.GenerateSql());
        }
    }
}