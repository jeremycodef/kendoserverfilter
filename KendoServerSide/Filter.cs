using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace KendoServerSide
{
    public class Filter
    {
        public string logic { get; set; }
        public List<Filter>? filters { get; set; }
        public string? field { get; set; }
        public string? @operator { get; set; }

        public object? value { get; set; }

        public string FormatValue()
        {
            if (this.value.GetType() == typeof(System.String))
            {
                return $"'{value}'";
            }

            if (this.value.GetType().FullName.StartsWith("System.Int"))
            {
                return $"{value}";
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public string GenerateSql()
        {
            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < filters.Count; i++)
            {
                if (filters[i].filters != null)
                {
                    builder.Append(filters[i].GenerateSql());
                    continue;
                }
                builder.Append(filters[i].field);
                builder.Append(' ');
                builder.Append(ParseOperator(filters[i].@operator));
                builder.Append(' ');
                if (i != filters.Count - 1)
                {
                    builder.Append(filters[i].FormatValue());
                    builder.Append(' ');
                }
                else
                {
                    builder.Append(filters[i].FormatValue());
                }
                if (i != filters.Count - 1)
                {
                    builder.Append(logic.ToUpper());
                    builder.Append(' ');
                }
            }

            return builder.ToString();
        }

        private string ParseOperator(string op)
        {
            // eq” (equal to), “neq” (not equal to), “lt” (less than), “lte” (less than or equal to), “gt” (greater than), “gte” (greater than or equal to), “startswith”, “endswith”, “contains”, “doesnotcontain”.    
            switch (op)
            {
                case "eq":
                    return "=";
                case "neq":
                    return "!=";
                case "lt":
                    return "<";
                case "lte":
                    return "<=";
                case "gt":
                    return ">";
                case "gte":
                    return ">=";
                default:
                    throw new ArgumentException($"Operator {op} is not supported");
            }
        }
    }
}